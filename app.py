from flask import Flask
from flask import render_template
from flask import request
from transformers import pipeline
import random

nlp_token_class = pipeline('ner')
nlp_qa = pipeline('question-answering')

greeting_input_texts = ("hi","hey", "heys", "hello", "morning","afternoon", "evening","greetings",)
greeting_replie_texts = ["hey", "Ola!", "hello, hope you are doing well", "hello there", "Welcome, how are you",]

app = Flask(__name__)

@app.route("/")
def home():
    return "Hello User. Welcome to covid questionnaire"

@app.route("/chat")
def chat():
    return render_template("index.html")

@app.route("/chatmessage")
def chatmessage():
    msz = request.args.get("msz")

    pairs =[
    ['(hi|hello|hey|holla|hola)', ['Hey there!', 'Hi there!', 'Hey!']],
    ['what is your name?', ['My name is Lucy']],
    ['what do you do?', ['I am a chatbot that will answer your covid related queries. How may I help you?']],
    ['who created you?', ['Team Twinkling of GSOP']]
    ]

    for i in range(0,len(pairs)):
        if msz.lower() in pairs[i][0]:
            return str(random.choice(pairs[i][1]))

    for word in msz.split():
        if word.lower() in greeting_input_texts:
            return str(random.choice(greeting_replie_texts))
    
    questions = [msz]
    file = open("covid.txt",encoding="utf8")
    context = file.read()
    response = nlp_qa(context=[context]*len(questions), question=questions)
    if response["score"]*100>=20:
        return response["answer"]
    else:
        return "I'm sorry I have no idea. How else may I help you? "
        # print(str(response))
        # return response["answer"]


app.run(port=5000)